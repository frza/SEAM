
from openmdao.main.api import Assembly
from openmdao.lib.datatypes.api import Float, Enum, Array, Int, Bool

# from SEAMAero.aero_design import AerodynamicDesign
from SEAMCosts.SEAMCAPEX import SEAMCAPEX
from SEAMCosts.costs_scaling import OPEXSimple
from SEAMLoads.SEAMLoads import SEAMLoads
from SEAMTower.SEAMTower import SEAMTower
from SEAMAero.SEAM_AEP import SEAMAEP
from SEAMRotor.SEAMRotor import SEAMBladeStructure
# from SEAMGeometry.SEAMGeometry import SEAMGeometry

def connect_io(top, cls):

    cls_name = cls.name
    for name in cls.list_inputs():

        try:
            top.connect(name, cls_name + '.%s' % name)
        except:
            # print 'failed connecting', cls_name, name
            pass

    for name in cls.list_outputs():
        try:
            top.connect(cls_name + '.%s' % name, name)
        except:
            pass


class SEAMAssembly(Assembly):

    d2e = Float(0.73, iotype='in', desc='Dollars to Euro ratio')
    rated_power = Float(3., iotype='in', units='MW', desc='Turbine rated power')
    hub_height = Float(100., iotype='in', units='m', desc='Hub height')
    rotor_diameter = Float(110., iotype='in', units='m', desc='Rotor diameter')
    site_type = Enum('onshore', values=('onshore', 'offshore'), iotype='in',
                                desc='Site type: onshore or offshore')
    tower_cost_per_mass = Float(4.0, iotype='in', desc='Tower cost per mass')
    blade_cost_per_mass = Float(15., iotype='in', desc='Blade cost per mass')
    hub_cost_per_mass = Float(3.5, iotype='in', desc='Hub cost per mass')
    spinner_cost_per_mass = Float(4.5, iotype='in', desc='Spinner cost per mass')
    bearing_cost_per_mass = Float(14.0, iotype='in', desc='Bearing cost per mass')

    turbine_cost = Float(iotype='out', desc='Total turbine CAPEX')
    infra_cost = Float(iotype='out', desc='Total infrastructure CAPEX')
    total_cost = Float(iotype='out', desc='Total CAPEX')

    rho_steel = Float(7.8e3, iotype='in', desc='density of steel')
    D_bottom = Float(4., iotype='in', desc='Tower bottom diameter')
    D_top = Float(2., iotype='in', desc='Tower top diameter')

    Neq = Float(1.e7, iotype='in', desc='')
    Slim_ext = Float(235., iotype='in', units='MPa', desc='')
    Slim_fat = Float(14.885, iotype='in', units='MPa', desc='')
    SF_tower = Float(1.5, iotype='in', desc='')
    PMtarget = Float(1., iotype='in', desc='')
    WohlerExpTower = Float(4., iotype='in', desc='')

    height = Array(iotype='out', desc='Tower discretization')
    t = Array(iotype='out', desc='Wall thickness')
    mass = Float(iotype='out', desc='Tower mass')

    tsr = Float(iotype='in', units='m', desc='Design tip speed ratio')
    F = Float(iotype='in', desc='')
    WohlerExpFlap = Float(iotype='in', desc='Wohler Exponent blade flap')
    WohlerExpTower = Float(iotype='in', desc='Wohler Exponent tower bottom')
    nSigma4fatFlap = Float(iotype='in', desc='')
    nSigma4fatTower = Float(iotype='in', desc='')
    dLoaddUfactorFlap = Float(iotype='in', desc='')
    dLoaddUfactorTower = Float(iotype='in', desc='')
    Neq = Float(iotype='in', desc='')
    EdgeExtDynFact = Float(iotype='in', desc='')
    EdgeFatDynFact = Float(iotype='in', desc='')

    max_tipspeed = Float(iotype='in', desc='Maximum tip speed')
    n_wsp = Int(iotype='in', desc='Number of wind speed bins')
    min_wsp = Float(0.0, iotype = 'in', units = 'm/s', desc = 'min wind speed')
    max_wsp = Float(iotype = 'in', units = 'm/s', desc = 'max wind speed')

    Iref = Float(iotype='in', desc='Reference turbulence intensity')
    WeibullInput = Bool(iotype='in', desc='Flag for Weibull input')
    WeiA_input = Float(iotype = 'in', units='m/s', desc = 'Weibull A')
    WeiC_input = Float(iotype = 'in', desc='Weibull C')
    NYears = Float(iotype = 'in', desc='Operating years')

    overallMaxTower = Float(iotype='out', units='kN*m', desc='Max tower bottom moment')
    overallMaxFlap = Float(iotype='out', units='kN*m', desc='Max blade root flap moment')
    FlapLEQ = Float(iotype='out', units='kN*m', desc='Blade root flap lifetime eq. moment')
    TowerLEQ = Float(iotype='out', units='kN*m', desc='Tower bottom lifetime eq. moment')

    Nsections = Int(iotype='in', desc='number of sections')
    Neq = Float(1.e7, iotype='in', desc='')

    WohlerExpFlap = Float(iotype='in', desc='')
    PMtarget = Float(iotype='in', desc='')

    rotor_diameter = Float(iotype='in', units='m', desc='') #[m]
    MaxChordrR = Float(iotype='in', units='m', desc='') #[m]

    OverallMaxFlap = Float(iotype='in', desc='')
    OverallMaxEdge = Float(iotype='in', desc='')
    TIF_FLext = Float(iotype='in', desc='') # Tech Impr Factor _ flap extreme
    TIF_EDext = Float(iotype='in', desc='')

    FlapLEQ = Float(iotype='in', desc='')
    EdgeLEQ = Float(iotype='in', desc='')
    TIF_FLfat = Float(iotype='in', desc='')

    sc_frac_flap = Float(iotype='in', desc='') # sparcap fraction of chord flap
    sc_frac_edge = Float(iotype='in', desc='') # sparcap fraction of thickness edge

    SF_blade = Float(iotype='in', desc='') #[factor]
    Slim_ext_blade = Float(iotype='in', units='MPa', desc='')
    Slim_fat_blade = Float(iotype='in', units='MPa', desc='')

    AddWeightFactorBlade = Float(iotype='in', desc='') # Additional weight factor for blade shell
    BladeDens = Float(iotype='in', units='kg/m**3', desc='density of blades') # [kg / m^3]
    BladeCostPerMass = Float(iotype='in', desc='') #[e/kg]
    HubCostPerMass = Float(iotype='in', desc='') #[e/kg]
    SpinnerCostPerMass = Float(iotype='in', desc='') #[e/kg]

    BladeWeight = Float(iotype = 'out', units = 'kg', desc = 'BladeMass' )

    mean_wsp = Float(iotype = 'in', units = 'm/s', desc = 'mean wind speed')    # [m/s]
    air_density = Float(iotype = 'in', units = 'kg/m**3', desc = 'density of air') # [kg / m^3]
    turbulence_int = Float(iotype = 'in', desc = '')
    max_Cp = Float(iotype = 'in', desc = 'max CP')
    gearloss_const = Float(iotype = 'in', desc = 'Gear loss constant')
    gearloss_var = Float(iotype = 'in', desc = 'Gear loss variable')
    genloss = Float(iotype = 'in', desc = 'Generator loss')
    convloss = Float(iotype = 'in', desc = 'Converter loss')

    # Outputs
    rated_wind_speed = Float(units = 'm / s', iotype='out', desc='wind speed for rated power')
    ideal_power_curve = Array(iotype='out', units='kW', desc='total power before losses and turbulence')
    power_curve = Array(iotype='out', units='kW', desc='total power including losses and turbulence')
    wind_curve = Array(iotype='out', units='m/s', desc='wind curve associated with power curve')

    NYears = Float(iotype = 'in', desc='Operating years')  # move this to COE calculation

    aep = Float(iotype = 'out', units='mW*h', desc='Annual energy production in mWh')
    total_aep = Float(iotype = 'out', units='mW*h', desc='AEP for total years of production')


    def configure(self):

        # self.add('aero', AerodynamicDesign())
        # self.driver.workflow.add('aero')
        #
        self.add('aep_calc', SEAMAEP())
        self.add('loads', SEAMLoads())
        self.add('tower_design', SEAMTower(21))
        self.add('blade_design', SEAMBladeStructure())
        self.driver.workflow.add(['aep_calc', 'loads', 'tower_design', 'blade_design'])

        self.connect('loads.overallMaxTower', 'tower_design.overallMaxTower')
        self.connect('loads.TowerLEQ', 'tower_design.TowerLEQ')

        self.connect('loads.overallMaxFlap', 'blade_design.overallMaxFlap')
        self.connect('loads.overallMaxEdge', 'blade_design.overallMaxEdge')
        self.connect('loads.FlapLEQ', 'blade_design.FlapLEQ')
        self.connect('loads.EdgeLEQ', 'blade_design.EdgeLEQ')

        # self.add('capex', SEAMCAPEX())
        # self.driver.workflow.add('capex')
        #
        # self.add('opex', OPEXSimple())
        # self.driver.workflow.add('opex')
        #


        connect_io(self, self.aep_calc)
        connect_io(self, self.loads)
        connect_io(self, self.tower_design)
        connect_io(self, self.blade_design)
