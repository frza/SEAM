
import unittest

from SEAM.seam_assemblies import SEAMAssembly

top = SEAMAssembly()

top.BladeCostPerMass = 15.0
top.HubCostPerMass = 3.5
top.SpinnerCostPerMass = 4.5
top.hub_cost_per_mass = 3.5
top.spinner_cost_per_mass = 4.5
top.tower_cost_per_mass = 4.0

top.AddWeightFactorBlade = 1.2
top.BladeDens = 2100.0
top.D_bottom = 8.3
top.D_top = 5.5
top.EdgeExtDynFact = 2.5
top.EdgeFatDynFact = 0.75
top.F = 0.777
top.Iref = 0.16
top.MaxChordrR = 0.2
top.NYears = 20.0
top.Neq = 10000000.0
top.Nsections = 21
top.PMtarget = 1.0
top.SF_blade = 1.1
top.SF_tower = 1.5
top.Slim_ext = 235.0
top.Slim_fat = 14.885
top.Slim_ext_blade = 200.0
top.Slim_fat_blade = 27.0
top.TIF_EDext = 1.0
top.TIF_FLext = 1.0
top.TIF_FLfat = 1.0
top.WeiA_input = 11.0
top.WeiC_input = 2.0
top.WeibullInput = True
top.WohlerExpFlap = 10.0
top.WohlerExpTower = 4.0
top.bearing_cost_per_mass = 14.0
top.blade_cost_per_mass = 15.0
top.d2e = 0.73
top.dLoaddUfactorFlap = 0.9
top.dLoaddUfactorTower = 0.8
top.hub_height = 120.0
top.max_tipspeed = 90.0
top.n_wsp = 26
top.min_wsp = 0.0
top.max_wsp = 25.0
top.nSigma4fatFlap = 1.2
top.nSigma4fatTower = 0.8
top.rated_power = 10.0
top.rho_steel = 7800.0
top.rotor_diameter = 178.0
top.sc_frac_edge = 0.8
top.sc_frac_flap = 0.3
top.site_type = 'onshore'
top.tsr = 8.0
top.air_density = 1.225
top.turbulence_int = 0.1
top.max_Cp = 0.49
top.gearloss_const = 0.01    # Fraction
top.gearloss_var = 0.014     # Fraction
top.genloss = 0.03          # Fraction
top.convloss = 0.03         # Fraction


top.run()

# class SEAMTestCase(unittest.TestCase) =
# 
#     def setUp(self) =
#         pass
#         
#     def tearDown(self) =
#         pass
#         
#     # add some tests here...
#     
#     #def test_SEAM(self) =
#         #pass
#         
# if __name__ == "__main__" =
#     unittest.main()
