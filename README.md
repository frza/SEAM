# DTU Wind Energy Systems Engineering Analysis Model (SEAM)


## Prerequisites

SEAM is written primarily in Python and uses [OpenMDAO](http://openmdao.org/) as the underlying framework for connecting its sub-models.
Additionally, SEAM uses components and interfaces defined in [FUSED-Wind](http://fusedwind.org/), which is a plugin to OpenMDAO.
Before installing SEAM, you therefore need to install these two packages according to their installation instructions.

## Installation

To install SEAM you can follow the standard intructions for installing an OpenMDAO plugin.

First, clone the main SEAM repository:

    $ git clone https://gitlab.windenergy.dtu.dk/SEAM/SEAM.git
    
Navigate to its root directory and install:

    $ cd SEAM
    $ <activate OpenMDAO environment>
    $ plugin install
    
To test that SEAM was installed correctly run the following

    $ python
    >>> import SEAM
    
To run the unit tests run

    $ python -m unittest discover 'src/SEAM/test' 'test_*.py'
    
To build and view the Sphinx documentation issue the following commands from the SEAM root directory:

    $ plugin build_docs SEAM
    $ plugin docs SEAM
    
